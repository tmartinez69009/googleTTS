<?php

/* This file is part of Jeedom.
*
* Jeedom is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Jeedom is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Jeedom. If not, see <http://www.gnu.org/licenses/>.
*/

/* * ***************************Includes********************************* */
require_once dirname(__FILE__) . '/../../../../core/php/core.inc.php';

class googleTTS extends eqLogic {

	public static function updateGoogleTTS() {
		log::remove('googleTTS_update');
		$cmd = 'sudo /bin/bash ' . dirname(__FILE__) . '/../../ressources/install.sh';
		$cmd .= ' >> ' . log::getPathToLog('googleTTS_update') . ' 2>&1 &';
		exec($cmd);
	}
	
	public static $_dico = array(
		'af'=>'af-ZA',//Afrikaans
		'sq'=>'sq-AL',//Albanian
		'ar'=>'ar-YE',//Arabic
		'hy'=>'hy-AM',//Armenian
		'ca'=>'ca-ES',//Catalan
		'zh-CN'=>'zh-CN',//Mandarin (simplified)
		'zh-TW'=>'zh-TW',//Mandarin (traditional)
		'hr'=>'hr-HR',//Croatian
		'cs'=>'cs-CZ',//Czech
		'da'=>'da-DK',//Danish
		'nl'=>'nl-NL',//Dutch
		'en'=>'en-GB',//English
		'en-us'=>'en-US',//English (United States)
		'en-au'=>'en-AU',//English (Australia)
		'eo'=>'eo',//Esperanto
		'fi'=>'fi-FI',//Finnish
		'fr'=>'fr-FR',//French
		'de'=>'de-DE',//German
		'el'=>'el-GR',//Greek
		'ht'=>'ht',//Haitian Creole
		'hi'=>'hi-IN',//Hindi
		'hu'=>'hu-HU',//Hungarian
		'is'=>'is-IS',//Icelandic
		'id'=>'id-ID',//Indonesian
		'it'=>'it-IT',//Italian
		'ja'=>'ja-JP',//Japanese
		'ko'=>'ko-KR',//Korean
		'la'=>'la',//Latin
		'lv'=>'lv-LV',//Latvian
		'mk'=>'mk-MK',//Macedonian
		'no'=>'nb-NO',//Norwegian
		'pl'=>'pl-PL',//Polish
		'pt'=>'pt-PT',//Portuguese
		'ro'=>'ro-RO',//Romanian
		'ru'=>'ru-RU',//Russian
		'sr'=>'sr-SP',//Serbian
		'sk'=>'sk-SK',//Slovak
		'es'=>'es-ES',//Spanish
		'sw'=>'sw-KE',//Swahili
		'sv'=>'sv-SE',//Swedish
		'ta'=>'ta-IN',//Tamil
		'th'=>'th-TH',//Thai
		'tr'=>'tr-TR',//Turkish
		'vi'=>'vi-VN',//Vietnamese
		'cy'=>'cy-GB',//Welsh
	         );
	
    /*     * *********************Methode d'instance************************* */
	
	public function postUpdate() {
			$googleTTSCmd = $this->getCmd(null, 'parle');
			if (!is_object($googleTTSCmd)) {
				$googleTTSCmd = new googleTTSCmd();
			}
	        $googleTTSCmd->setName(__('Parle', __FILE__));
			$googleTTSCmd->setLogicalId('parle');
	        $googleTTSCmd->setEqLogic_id($this->getId());
	        $googleTTSCmd->setType('action');
	        $googleTTSCmd->setSubType('message');
	        $googleTTSCmd->save();
    }
	public function toHtml($_version = 'dashboard') {
        if ($this->getIsEnable() != 1) {
            return '';
        }
		
        $cmd_id="";
		$id=array();
		foreach($this->getCmd() as $cmd){
			$cmd_id=$cmd->getId();
		}
		$replace = array(
        		'#name#' => $this->getName(),
            	'#id#' => $this->getId(),
            	'#cmd_id#'=>$cmd_id,
            	'#background_color#' => $this->getBackgroundColor(jeedom::versionAlias($_version)),
            	'#eqLink#' => $this->getLinkToConfiguration(),
        	);
        
		if (($_version == 'dview' || $_version == 'mview') && $this->getDisplay('doNotShowNameOnView') == 1) {
            $replace['#name#'] = '';
            $replace['#object_name#'] = (is_object($object)) ? $object->getName() : '';
        }
        if (($_version == 'mobile' || $_version == 'dashboard') && $this->getDisplay('doNotShowNameOnDashboard') == 1) {
            $replace['#name#'] = '<br/>';
            $replace['#object_name#'] = (is_object($object)) ? $object->getName() : '';
        }
		$parameters = $this->getDisplay('parameters');
        if (is_array($parameters)) {
            foreach ($parameters as $key => $value) {
                $replace['#' . $key . '#'] = $value;
            }
        }
        
        return template_replace($replace, getTemplate('core', jeedom::versionAlias($_version), 'tts', 'googleTTS'));	
        
    }

}

class googleTTSCmd extends cmd {

    /*     * *********************Methode d'instance************************* */

	public function dontRemoveCmd() {
        return true;
    }
    
    public function execute($_options = null) {
    	
		$googleTTS = $this->getEqLogic();
		if($googleTTS->getConfiguration('lang')==""){
			$googleTTS_lang = "fr";
		}else{	
			$googleTTS_lang = $googleTTS->getConfiguration('lang');
		}
		if($googleTTS_moteurTTS=="pico"){
			$googleTTS_lang = self::$_dico[$googleTTS_lang];
		}
		$googleTTS_opt = $googleTTS->getConfiguration('opt');
		$googleTTS_moteurTTS = $googleTTS->getConfiguration('moteurTTS');
		if($googleTTS_moteurTTS=="url"){
			$googleTTS_url = $googleTTS->getConfiguration('url');
		}else if($googleTTS_moteurTTS=="pico") {
		    $googleTTS_url = "pico";
		} else {
			$googleTTS_url = "";
		}
		if ($googleTTS->getConfiguration('maitreesclave') == 'deporte'){
			$googleTTS_path = $googleTTS->getConfiguration('chemin');
		}else {
			$googleTTS_path = realpath(dirname(__FILE__) . '/../../ressources');
		}
		$response = true;

		if (is_numeric($_options['title']) && $_options['title']>=0 && $_options['title']<=100){
			$volume=$_options['title'];
		} else {
			$volume=100;
		}
		if ($googleTTS_opt==''){
			$googleTTS_opt='-volume '.$volume;
		} else {
			$googleTTS_opt=$googleTTS_opt.' -volume '.$volume;
		}
		log::add('googleTTS', 'info', 'Debut de l action '.'/usr/bin/python ' . $googleTTS_path . '/tts.py -l '.$googleTTS_lang.' -o "'.$googleTTS_opt.'" -u "'.$googleTTS_url.'" -t "' . $_options['message'] . '" 2>&1');
		if ($googleTTS->getConfiguration('maitreesclave') == 'deporte'){
			$ip=$googleTTS->getConfiguration('addressip');
			$port=$googleTTS->getConfiguration('portssh');
			$user=$googleTTS->getConfiguration('user');
			$pass=$googleTTS->getConfiguration('password');
			if (!$connection = ssh2_connect($ip,$port)) {
				log::add('googleTTS', 'error', 'connexion SSH KO');
			}else{
				if (!ssh2_auth_password($connection,$user,$pass)){
				log::add('googleTTS', 'error', 'Authentification SSH KO');
				}else{
					$result = ssh2_exec($connection, '/usr/bin/python ' . $googleTTS_path . '/tts.py -l '.$googleTTS_lang.' -o "'.$googleTTS_opt.'" -u "'.$googleTTS_url.'" -t "' . $_options['message'] . '" 2>&1');
					stream_set_blocking($result, true);
					$result = stream_get_contents($result);
					
					$closesession = ssh2_exec($connection, 'exit'); 
					stream_set_blocking($closesession, true);
					stream_get_contents($closesession);
				}
			}
		}else {
			$result = shell_exec('/usr/bin/python ' . $googleTTS_path . '/tts.py -l '.$googleTTS_lang.' -o "'.$googleTTS_opt.'" -u "'.$googleTTS_url.'" -t "' . $_options['message'] . '" 2>&1');
		}
		return $result;

    /*     * **********************Getteur Setteur*************************** */
   }
}
?>
