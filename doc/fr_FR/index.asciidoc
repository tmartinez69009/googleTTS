== GoogleTTS

=== Description
Plugin pour faire du TTS via Google. 
image:../images/googleTTS_screenshot1.png[]

'''
=== Installation
include::installation.asciidoc[]

'''
=== Configuration
include::configuration.asciidoc[]

'''
=== Changelog
include::changelog.asciidoc[]
